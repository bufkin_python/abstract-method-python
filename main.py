#! /usr/bin/env/python311

from abc import ABC, abstractmethod
import logging


class Phone(ABC):
    def __init__(self, model: str):
        pass

    @property
    @abstractmethod
    def power(self):
        logging.warning('Power on')

    def call_target(self, name: str):
        logging.warning('Call target')


class iBanana(Phone):
    def __init__(self, model: str):
        super().__init__(model)

    @property
    def power(self):
        logging.info('50% power remaining')

    def call_target(self, name: str):
        logging.warning('Error in call target')
        print("Error")


if __name__ == '__main__':
    logging.basicConfig(filename='example.log',
                        encoding='utf-8', level=logging.DEBUG)

    ibanana = iBanana('iBanana')
    print(ibanana.power)
    ibanana.call_target('Luigi')
